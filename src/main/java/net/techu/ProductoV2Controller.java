package net.techu;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ProductoV2Controller {

    private ArrayList<Producto> listaProductos = null;

    public ProductoV2Controller() {
        listaProductos = new ArrayList<>();
        listaProductos.add(new Producto(1, "Pr1", 123.822));
        listaProductos.add(new Producto(2, "Pr2", 333.72));
        listaProductos.add(new Producto(3, "Pr3", 6673.42));
    }

    /* Get lista de productos */
    @GetMapping(value = "/v2/productos")
    public ResponseEntity<List<Producto>> getList() {
        System.out.println("Estoy en obtener");
        return new ResponseEntity<>(listaProductos, HttpStatus.OK);
    }

    @GetMapping("/v2/productos/{id}")
    public ResponseEntity<Producto> getListId(@PathVariable int id) {
        Producto resultado = null;
        ResponseEntity<Producto> respuesta = null;
        try {
            resultado = listaProductos.get(id);
            respuesta = new ResponseEntity<>(resultado, HttpStatus.OK);
        } catch (Exception ex) {
            respuesta = new ResponseEntity(resultado, HttpStatus.NOT_FOUND);
        }
        return respuesta;
    }

    /* Add nuevo producto */
    @PostMapping(value = "/v2/productos")
    public ResponseEntity<String> addProducto(@RequestBody Producto producto) {
        System.out.println("Estoy en añadir " + producto.getName());
        listaProductos.add(producto);
        return new ResponseEntity<>("Producto creado correctamente", HttpStatus.CREATED);
    }

    /* Add nuevo producto con nombre */
    @PostMapping(value = "/v2/productos/{nom}/{cat}", produces = "application/json")
    public ResponseEntity<String> addProductWithName(@PathVariable("nom") String nombre, @PathVariable("cat") String categoria) {
        System.out.println("Voy a añadir el producto con nombre " + nombre + " y categoría " + categoria);
        listaProductos.add(new Producto(99, "NUEVO", 100.5));
        return new ResponseEntity<>("Producto creado correctamente", HttpStatus.CREATED);
    }

    @PutMapping("/v2/productos/{id}")
    public ResponseEntity<String> updateProduct(@PathVariable int id, @RequestBody Producto producto) {
        ResponseEntity<String> resultado = null;
        try {
            Producto productoAModificar = listaProductos.get(id);
            System.out.println("Voy a modificar el producto");
            System.out.println("Precio actual: " + String.valueOf(productoAModificar.getPrice()));
            System.out.println("Precio nuevo: " + String.valueOf(producto.getPrice()));
            productoAModificar.setName(producto.getName());
            productoAModificar.setPrice(producto.getPrice());
            listaProductos.set(id, productoAModificar);
            resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            resultado = new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
        return resultado;
    }

    @PutMapping("/v2/productos")
    public ResponseEntity<String> priceUp() {
        ResponseEntity<String> resultado = null;
        for (Producto p : listaProductos) {
            p.setPrice(p.getPrice() * 1.25);
        }
        resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return resultado;
    }

    @DeleteMapping("/v2/productos/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable int id) {
        ResponseEntity<String> resultado = null;
        try {
            Producto productoAEliminar = listaProductos.get(id - 1);
            listaProductos.remove(productoAEliminar);
            resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            resultado = new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
        return resultado;
    }

    @DeleteMapping("/v2/productos")
    public ResponseEntity<String> deleteProductAll() {
        ResponseEntity<String> resultado = null;
        listaProductos.removeAll(listaProductos);
        resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return resultado;
    }
}
