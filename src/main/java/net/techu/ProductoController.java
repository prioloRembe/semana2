package net.techu;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ProductoController {

    private ArrayList<String> listaProductos = null;

    public ProductoController() {
        listaProductos = new ArrayList<>();
        listaProductos.add("Pr1");
        listaProductos.add("Pr2");
    }

    @GetMapping("/productos")
    public ResponseEntity<List<String>> obtenerListado() {
        System.out.println("Estoy en obtener");
        return new ResponseEntity<>(listaProductos, HttpStatus.OK);
    }

    @GetMapping("/productos/{id}")
    public ResponseEntity<String> getProductId(@PathVariable("id") int id) {
        String resultado = null;
        ResponseEntity<String> respuesta = null;
        try {
            resultado = listaProductos.get(id);
            respuesta = new ResponseEntity<>(resultado, HttpStatus.OK);
        } catch (Exception ex) {
            resultado = "No se ha encontrado el producto: " + id;
            respuesta = new ResponseEntity<>(resultado, HttpStatus.NOT_FOUND);
        }
        return respuesta;
    }

    @PostMapping("/productos")
    public ResponseEntity<String> addProduct() {
        listaProductos.add("nuevo");
        return new ResponseEntity<>("Creado", HttpStatus.CREATED);
    }

    @PostMapping("/productos/{name}/{cat}")
    public void addProductName(@PathVariable("name") String name, @PathVariable("cat") String cat) {
        System.out.println("Voy a añadir el producto con nombre " + name + " y categoría " + cat);
        listaProductos.add(name);
    }

    @PutMapping("/productos/{id}")
    public ResponseEntity<String> updateProduct(@PathVariable int id) {
        ResponseEntity<String> resultado = null;
        try {
            String productoAModificar = listaProductos.get(id);
            resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            resultado = new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
        return resultado;
    }

    @DeleteMapping("/productos/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable int id) {
        ResponseEntity<String> resultado = null;
        try {
            String productoAEliminar = listaProductos.get(id);
            resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            resultado = new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
        return resultado;
    }
}
