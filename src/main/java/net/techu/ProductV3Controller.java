package net.techu;

import net.techu.data.ProductMongo;
import net.techu.data.ProductRepositiry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
public class ProductV3Controller implements CommandLineRunner {

    @Autowired
    private ProductRepositiry repositiry;

    public static void main(String[] args) {
        SpringApplication.run(ProductV3Controller.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Preparando MongoDB");
        repositiry.deleteAll();
        repositiry.insert(new ProductMongo("Product1", 99));
        repositiry.insert(new ProductMongo("Product2", 45));

        List<ProductMongo> list = repositiry.findAll();
        for (ProductMongo p : list) {
            System.out.println(p.toString());
        }
    }
}
