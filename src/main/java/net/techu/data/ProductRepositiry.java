package net.techu.data;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProductRepositiry extends MongoRepository<ProductMongo,String> {

    public ProductMongo findByName(String name);
}
