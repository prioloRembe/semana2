package net.techu.data;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("productsRemberto")
public class ProductMongo {

    @Id
    public String id;
    public String name;
    public double price;

    public ProductMongo() {
    }

    public ProductMongo(String name, double price) {
        this.name = name;
        this.price = price;
    }

    @Override
    public String toString() {
        return "ProductoMongo{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", price='" + price + '\'' +
                '}';
    }
}
